USE [DigitaleAssistenter]
GO
/****** Object:  Table [dbo].[Attachment]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attachment](
	[AttachmentId] [int] IDENTITY(1,1) NOT NULL,
	[ProcessId] [int] NOT NULL,
	[FileName] [nvarchar](500) NOT NULL,
	[ContentType] [nvarchar](150) NOT NULL,
	[File] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_Attachment] PRIMARY KEY CLUSTERED 
(
	[AttachmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[Initials] [nvarchar](50) NULL,
	[Name] [nvarchar](200) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KLE]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KLE](
	[KLEId] [int] NOT NULL,
	[KLENumber] [nvarchar](8) NOT NULL,
	[KLENumberText] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_KLE] PRIMARY KEY CLUSTERED 
(
	[KLEId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization](
	[OrganizationId] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Process]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Process](
	[ProcessId] [int] IDENTITY(1,1) NOT NULL,
	[KLEId] [int] NULL,
	[ContactEmployeeId] [int] NOT NULL,
	[ReporterEmployeeId] [int] NOT NULL,
	[CreatedByEmployeeId] [int] NOT NULL,
	[ProfessionalAssessmentRatingId] [int] NOT NULL,
	[FrequentChangesRatingId] [int] NOT NULL,
	[StructuredDataRatingId] [int] NOT NULL,
	[VariationRatingId] [int] NOT NULL,
	[StatusId] [int] NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Resume] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ConfidentialProcess] [bit] NULL,
	[Paragraph] [nvarchar](300) NULL,
	[SolutionDescription] [nvarchar](max) NULL,
	[AmountOfProcessExecutionsYearly] [nvarchar](300) NOT NULL,
	[TimeOfExecution] [nvarchar](300) NULL,
	[ProcessExecutionTime] [nvarchar](300) NOT NULL,
	[ProcessExecutionTimeHoursYearly] [nvarchar](300) NULL,
	[AmountOfEmployees] [nvarchar](300) NOT NULL,
	[AmountOfCitizensOrCompaniesAffected] [nvarchar](300) NOT NULL,
	[IsDataAvailableInSystems] [nvarchar](300) NULL,
	[CurrentSystems] [nvarchar](300) NOT NULL,
	[AutomationWouldOptimizeTheProcess] [nvarchar](300) NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[EstimatedPotential] [nvarchar](300) NULL,
	[Experience] [nvarchar](max) NULL,
	[EstimatedStartDate] [datetime2](7) NULL,
	[EstimatedEndDate] [datetime2](7) NULL,
	[TypeOfSolution] [nvarchar](max) NULL,
	[SolutionTechnologies] [nvarchar](max) NULL,
	[Risk] [nvarchar](max) NULL,
	[AdditionalInformationProjectGroup] [nvarchar](max) NULL,
 CONSTRAINT [PK_Process] PRIMARY KEY CLUSTERED 
(
	[ProcessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProcessOrganization]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessOrganization](
	[ProcessOrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[ProcessId] [int] NOT NULL,
 CONSTRAINT [PK_ProcessOrganization] PRIMARY KEY CLUSTERED 
(
	[ProcessOrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rating]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rating](
	[RatingId] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](50) NOT NULL,
	[Score] [int] NULL,
 CONSTRAINT [PK_Rating] PRIMARY KEY CLUSTERED 
(
	[RatingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[EmployeeView]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[EmployeeView]
AS

SELECT [EmployeeId]
      ,[Initials]
      ,[Name]
	  ,InitialsAndName = [Name] + ' (' + [Initials] + ')'
  FROM [DigitaleAssistenter].[dbo].[Employee]

GO
/****** Object:  View [dbo].[KLEView]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[KLEView]
AS

SELECT [KLEId]
      ,[KLENumber]
      ,[KLENumberText]
  FROM [DigitaleAssistenter].[dbo].[KLE]

GO
/****** Object:  View [dbo].[OrganizationView]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[OrganizationView]
AS

SELECT [OrganizationId]
      ,[Name]
  FROM [DigitaleAssistenter].[dbo].[Organization]

GO
/****** Object:  View [dbo].[ProcessView]    Script Date: 29-11-2017 11:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[ProcessView]
AS


SELECT	   p.[ProcessId]
		  ,p.[KLEId]
		  ,kle.[KLENumberText] as KLE
		  ,p.[ContactEmployeeId]
		  ,contact.[Name] as Contact
		  ,p.[ReporterEmployeeId]
		  ,reporter.[Name] as Reporter
		  ,p.[CreatedByEmployeeId]		  
		  ,creator.[Name] as Creator
		  ,p.[ProfessionalAssessmentRatingId]
		  ,professionalAssesment.[Text] as ProfessionalAssesmentRating
		  ,p.[FrequentChangesRatingId]
		  ,frequentChanges.[Text] as FrequentChangesRating
		  ,p.[StructuredDataRatingId]
		  ,structuredData.[Text] as StructuredDataRating
		  ,p.[VariationRatingId]
		  ,variationRating.[Text] as VariationRating
		  ,p.[StatusId]
		  ,s.[Text] as [Status]
		  ,p.[Name]
    	  ,p.[Resume]
		  ,p.[Description]
	      ,p.[ConfidentialProcess]
		  ,p.[Paragraph]
		  ,p.[SolutionDescription]
		  ,p.[AmountOfProcessExecutionsYearly]
		  ,p.[TimeOfExecution]
		  ,p.[ProcessExecutionTime]
		  ,p.[ProcessExecutionTimeHoursYearly]
		  ,p.[AmountOfEmployees]
		  ,p.[AmountOfCitizensOrCompaniesAffected]
		  ,p.[IsDataAvailableInSystems]
		  ,p.[CurrentSystems]
		  ,p.[AutomationWouldOptimizeTheProcess]
		  ,p.[AdditionalInformation]
		  ,p.[EstimatedPotential]
		  ,p.[Experience]
		  ,p.[EstimatedStartDate]
		  ,p.[EstimatedEndDate]
	      ,p.[TypeOfSolution]
		  ,p.[SolutionTechnologies]
		  ,p.[Risk]
		  ,p.[AdditionalInformationProjectGroup]	
		  ,STUFF((SELECT ', ' + CAST(o.[Name] as varchar(max)) 
				   FROM [DigitaleAssistenter].[dbo].[ProcessOrganization] po
				   INNER JOIN [DigitaleAssistenter].[dbo].[OrganizationView] o
				   ON po.OrganizationId = o.OrganizationId
				   WHERE po.ProcessId = p.ProcessId
				   FOR XML PATH('')), 1, 2, '') as Organizations

FROM            dbo.Process p

LEFT JOIN [DigitaleAssistenter].[dbo].[Status] s
ON p.StatusId = s.StatusId

LEFT JOIN [DigitaleAssistenter].[dbo].[Rating] professionalAssesment
ON p.ProfessionalAssessmentRatingId = professionalAssesment.RatingId

LEFT JOIN [DigitaleAssistenter].[dbo].[Rating] frequentChanges
ON p.FrequentChangesRatingId = frequentChanges.RatingId

LEFT JOIN [DigitaleAssistenter].[dbo].[Rating] structuredData
ON p.StructuredDataRatingId = structuredData.RatingId

LEFT JOIN [DigitaleAssistenter].[dbo].[Rating] variationRating
ON p.VariationRatingId = variationRating.RatingId

LEFT JOIN [DigitaleAssistenter].[dbo].[EmployeeView] contact
ON p.ContactEmployeeId = contact.EmployeeId

LEFT JOIN [DigitaleAssistenter].[dbo].[EmployeeView] reporter
ON p.ReporterEmployeeId = reporter.EmployeeId

LEFT JOIN [DigitaleAssistenter].[dbo].[EmployeeView] creator
ON p.CreatedByEmployeeId = creator.EmployeeId

LEFT JOIN [DigitaleAssistenter].[dbo].[KLEView] kle
ON p.KLEId = kle.KLEId

GO
ALTER TABLE [dbo].[Process] ADD  CONSTRAINT [DF_Process_ConfidentialProcess]  DEFAULT ((0)) FOR [ConfidentialProcess]
GO
ALTER TABLE [dbo].[Attachment]  WITH CHECK ADD  CONSTRAINT [FK_Attachment_Process] FOREIGN KEY([ProcessId])
REFERENCES [dbo].[Process] ([ProcessId])
GO
ALTER TABLE [dbo].[Attachment] CHECK CONSTRAINT [FK_Attachment_Process]
GO
ALTER TABLE [dbo].[Process]  WITH CHECK ADD  CONSTRAINT [FK_Process_FrequentChanges_Rating] FOREIGN KEY([FrequentChangesRatingId])
REFERENCES [dbo].[Rating] ([RatingId])
GO
ALTER TABLE [dbo].[Process] CHECK CONSTRAINT [FK_Process_FrequentChanges_Rating]
GO
ALTER TABLE [dbo].[Process]  WITH CHECK ADD  CONSTRAINT [FK_Process_ProfessionalAssesment_Rating] FOREIGN KEY([ProfessionalAssessmentRatingId])
REFERENCES [dbo].[Rating] ([RatingId])
GO
ALTER TABLE [dbo].[Process] CHECK CONSTRAINT [FK_Process_ProfessionalAssesment_Rating]
GO
ALTER TABLE [dbo].[Process]  WITH CHECK ADD  CONSTRAINT [FK_Process_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([StatusId])
GO
ALTER TABLE [dbo].[Process] CHECK CONSTRAINT [FK_Process_Status]
GO
ALTER TABLE [dbo].[Process]  WITH CHECK ADD  CONSTRAINT [FK_Process_StructuredData_Rating] FOREIGN KEY([StructuredDataRatingId])
REFERENCES [dbo].[Rating] ([RatingId])
GO
ALTER TABLE [dbo].[Process] CHECK CONSTRAINT [FK_Process_StructuredData_Rating]
GO
ALTER TABLE [dbo].[Process]  WITH CHECK ADD  CONSTRAINT [FK_Process_Variation_Rating] FOREIGN KEY([VariationRatingId])
REFERENCES [dbo].[Rating] ([RatingId])
GO
ALTER TABLE [dbo].[Process] CHECK CONSTRAINT [FK_Process_Variation_Rating]
GO
ALTER TABLE [dbo].[ProcessOrganization]  WITH CHECK ADD  CONSTRAINT [FK_ProcessOrganization_Process] FOREIGN KEY([ProcessId])
REFERENCES [dbo].[Process] ([ProcessId])
GO
ALTER TABLE [dbo].[ProcessOrganization] CHECK CONSTRAINT [FK_ProcessOrganization_Process]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Employee_1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EmployeeView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EmployeeView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "KLE"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'KLEView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'KLEView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Organization"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'OrganizationView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'OrganizationView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Process"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 343
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProcessView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProcessView'

INSERT INTO [DigitaleAssistenter].[dbo].[Rating] ([Text], [Score])
VALUES ('I høj grad', 4),
       ('I nogen grad', 3),
	   ('I mindre grad', 2),
	   ('Slet ikke', 1),
	   ('Ved ikke', 0)

INSERT INTO [DigitaleAssistenter].[dbo].[Status] ([Text])
VALUES ('Idé'),
	   ('Foranalyse'),
	   ('Analyse'),
	   ('Udvikling') ,
	   ('Implementering'),
	   ('Drift')


GO
