# Digitale Assitenter - Indberetningsformular #

### Installationsvejledning ###

1. Opret ny database med navnet "DigitaleAssistenter"
2. Kør databasescriptet "DigitaleAssistenter.sql"
3. Opret ny databasebruger som har adgang til at læse og skrive i databasen
4. Opret ConnectionString.config ud fra skeleton-filen og indsæt servernavn, databasenavn, brugernavn og adgangskode
5. Opret ApplicationSettings.config ud fra skeleton-filen og indsæt Active Directory administratorgruppe
6. Indsæt medarbejder-, organisations- og KLE-data i tabellerne Employee, Organization og KLE
   eller
6. Ændr de tre views EmployeeView, OrganizationView og KLEView så de trækker på data fra egen datakilde (eks. SOFD)

Ved spørgsmål til ovenstående kontakt Kristian Hansen, kbha@syddjurs.dk. 

### License ###

Digitale Assistenter - Indberetningsformular was programmed by Kristian Hansen for Syddjurs Kommune (http://www.syddjurs.dk).

Copyright (c) 2017, Syddjurs Kommune.

Digitale Assistenter - Indberetningsformular is free software; you may use, study, modify and distribute it under the terms of version 2.0 of the Mozilla Public License. See the LICENSE file for details. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

All source code in this and the underlying directories is subject to the terms of the Mozilla Public License, v. 2.0.