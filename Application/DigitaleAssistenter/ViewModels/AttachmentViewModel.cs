﻿using System.ComponentModel.DataAnnotations;

namespace DigitaleAssistenter.ViewModels
{
    public class AttachmentViewModel
    {
        public int AttachmentId { get; set; }

        [Display(Name = "Proces")]
        [Required]
        public int ProcessId { get; set; }

        [Display(Name = "Filnavn")]
        [MaxLength(500)]
        [Required]
        public string FileName { get; set; }

        [Display(Name = "Filtype")]
        [MaxLength(150)]
        [Required]
        public string ContentType { get; set; }

        [Display(Name = "Fil")]
        [Required]
        public byte[] File { get; set; }
    }
}