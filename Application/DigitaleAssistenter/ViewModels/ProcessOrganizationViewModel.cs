﻿using System.ComponentModel.DataAnnotations;

namespace DigitaleAssistenter.ViewModels
{
    public class ProcessOrganizationViewModel
    {
        public int ProcessOrganizationViewModelId { get; set; }

        [Display(Name = "Proces")]
        [Required]
        public int ProcessId { get; set; }

        [Display(Name = "Organisation")]
        [Required]
        public int OrganizationId { get; set; }
    }
}