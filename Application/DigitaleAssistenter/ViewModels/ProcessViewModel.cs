﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using DigitaleAssistenter.Models;

namespace DigitaleAssistenter.ViewModels
{
    public class ProcessViewModel
    {
        [Display(Name = "Nr.")]
        public int ProcessId { get; set; }

        [Display(Name = "KLE-nummer")]
        public int? KLEId { get; set; }

        [Display(Name = "KLE-nummer")]
        public string KLE { get; set; }

        [Display(Name = "Fagområde/afdeling")]
        [Required]
        public string Organizations { get; set; }

        [Display(Name = "Kontaktperson i afdelingen")]
        [Required]
        public int ContactEmployeeId { get; set; }

        [Display(Name = "Kontaktperson i afdelingen")]
        public string Contact { get; set; }

        [Display(Name = "Proces indberetter")]
        [Required]
        public int ReporterEmployeeId { get; set; }

        [Display(Name = "Proces indberetter")]
        public string Reporter { get; set; }

        [Display(Name = "Oprettet af")]
        [Required]
        public int CreatedByEmployeeId { get; set; }

        [Display(Name = "Oprettet af")]
        public string CreatedBy { get; set; }

        [Display(Name = "I hvor høj grad indgår der faglig vurderinger i processen?", Description = "F.eks. at sagsbehandleren foretager et skøn eller tager en beslutning.")]
        [Required]
        public int ProfessionalAssessmentRatingId { get; set; }

        [Display(Name = "I hvilken grad er processen præget af hyppige ændringer", Description = "F.eks. ændret lovgivning")]
        [Required]
        public int FrequentChangesRatingId { get; set; }

        [Display(Name = "I hvor høj grad er processen baseret på strukturerede information?", Description = "F.eks. brug af en formular")]
        [Required]
        public int StructuredDataRatingId { get; set; }

        [Display(Name = "Er der variation i hvordan processen løses?")]
        [Required]
        public int VariationRatingId { get; set; }

        [Display(Name = "Status")]
        public int? StatusId { get; set; }

        [Display(Name = "Procesnavn")]
        [MaxLength(300)]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Resume")]
        [DataType(DataType.MultilineText)]
        public string Resume { get; set; }

        [Display(Name = "Fortrolig proces")]
        [Required]
        public bool ConfidentialProcess { get; set; }

        [Display(Name = "Paragraf")]
        [MaxLength(300)]
        public string Paragraph { get; set; }

        [Display(Name = "Beskrivelse af nuværende proces")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Beskrivelse af en mulig løsning")]
        [DataType(DataType.MultilineText)]
        public string SolutionDescription { get; set; }

        [Display(Name = "Nuværende systemer der understøtter processen")]
        [MaxLength(300)]
        [Required]
        public string CurrentSystems { get; set; }

        [Display(Name = "Antal gange processen foretages årligt")]
        [MaxLength(300)]
        [Required]
        public string AmountOfProcessExecutionsYearly { get; set; }

        [Display(Name = "Tidsforbrug pr. proces i minutter")]
        [MaxLength(300)]
        [Required]
        public string ProcessExecutionTime { get; set; }

        [Display(Name = "Antal medarbejdere der foretager processen")]
        [MaxLength(300)]
        [Required]
        public string AmountOfEmployees { get; set; }

        [Display(Name = "Total timeforbrug pr. år")]
        [MaxLength(300)]
        public string ProcessExecutionTimeHoursYearly { get; set; }

        [Display(Name = "Hvornår på året foretages processen")]
        [MaxLength(300)]
        public string TimeOfExecution { get; set; }

        [Display(Name = "Antal borgere/virksomheder der er berørte af processen")]
        [MaxLength(300)]
        [Required]
        public string AmountOfCitizensOrCompaniesAffected { get; set; }

        [Display(Name = "Er data og informationer, der skal bruges i processen tilgængelige digitalt i IT-systemer")]
        [MaxLength(300)]
        public string IsDataAvailableInSystems { get; set; }

        [Display(Name = "Kan automatisering bidrage til højere kvalitet eller højere service?")]
        [MaxLength(300)]
        public string AutomationWouldOptimizeTheProcess { get; set; }

        [Display(Name = "Andet", Description = "Noter gerne yderligere information omkring processen")]
        [DataType(DataType.MultilineText)]
        public string AdditionalInformation { get; set; }

        [Display(Name = "Bilag", Description = "F.eks. procestegninger, skærmbilleder eller anden dokumentation af processen. Hold CTRL nede for at markere flere filer.")]
        public IEnumerable<HttpPostedFileBase> UploadedFile { get; set; }

        [Display(Name = "Vurderet potentiale")]
        [MaxLength(300)]
        public string EstimatedPotential { get; set; }

        [Display(Name = "Erfaringer (evt. kontaktperson)")]
        [DataType(DataType.MultilineText)]
        public string Experience { get; set; }

        [Display(Name = "Forventet start")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EstimatedStartDate { get; set; }

        [Display(Name = "Forventet slut")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EstimatedEndDate { get; set; }

        [Display(Name = "Type af procesløsning")]
        [DataType(DataType.MultilineText)]
        public string TypeOfSolution { get; set; }

        [Display(Name = "Forslag til teknologi og løsning")]
        [DataType(DataType.MultilineText)]
        public string SolutionTechnologies { get; set; }

        [Display(Name = "Risiko og opmærksomhedspunkter")]
        [DataType(DataType.MultilineText)]
        public string Risk { get; set; }

        [Display(Name = "Yderligere oplysninger")]
        [DataType(DataType.MultilineText)]
        public string AdditionalInformationProjectGroup { get; set; }

        [Display(Name = "Bilag")]
        public virtual ICollection<AttachmentViewModel> Attachment { get; set; }

        [Display(Name = "Fagområde/afdeling")]
        public virtual ICollection<ProcessOrganizationViewModel> ProcessOrganization { get; set; }

        public virtual Rating FrequentChangesRating { get; set; }
        public virtual Rating ProfessionalAssesmentRating { get; set; }
        public virtual Rating StructuredDataRating { get; set; }
        public virtual Rating VariationRating { get; set; }
        public virtual Status Status { get; set; }
    }
}