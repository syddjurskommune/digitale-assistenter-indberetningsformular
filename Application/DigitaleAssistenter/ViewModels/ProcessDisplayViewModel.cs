﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace DigitaleAssistenter.ViewModels
{
    public class ProcessDisplayViewModel
    {
        [Display(Name = "Nr.")]
        public int ProcessId { get; set; }

        [Display(Name = "KLE-nummer")]
        public int? KLEId { get; set; }

        [Display(Name = "KLE-nummer")]
        public string KLE { get; set; }

        [Display(Name = "Fagområde/afdeling")]
        public string Organizations { get; set; }

        [Display(Name = "Kontaktperson i afdelingen")]
        public int ContactEmployeeId { get; set; }

        [Display(Name = "Kontaktperson i afdelingen")]
        public string Contact { get; set; }

        [Display(Name = "Proces indberetter")]
        public int ReporterEmployeeId { get; set; }

        [Display(Name = "Proces indberetter")]
        public string Reporter { get; set; }

        [Display(Name = "Oprettet af")]
        public int CreatedByEmployeeId { get; set; }

        [Display(Name = "Oprettet af")]
        public string CreatedBy { get; set; }

        [Display(Name = "I hvor høj grad indgår der faglig vurderinger i processen?", Description = "F.eks. at sagsbehandleren foretager et skøn eller tager en beslutning.")]
        public string ProfessionalAssesmentRating { get; set; }

        [Display(Name = "I hvilken grad er processen præget af hyppige ændringer", Description = "F.eks. ændret lovgivning")]
        public string FrequentChangesRating { get; set; }

        [Display(Name = "I hvor høj grad er processen baseret på strukturerede information?", Description = "F.eks. brug af en formular")]
        public string StructuredDataRating { get; set; }

        [Display(Name = "Er der variation i hvordan processen løses?")]
        public string VariationRating { get; set; }

        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        public string Status { get; set; }

        [Display(Name = "Procesnavn")]
        public string Name { get; set; }

        [Display(Name = "Resume")]
        [DataType(DataType.MultilineText)]
        public string Resume { get; set; }

        [Display(Name = "Fortrolig proces")]
        public bool ConfidentialProcess { get; set; }

        [Display(Name = "Paragraf")]
        public string Paragraph { get; set; }

        [Display(Name = "Beskrivelse af nuværende proces")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Beskrivelse af en mulig løsning")]
        [DataType(DataType.MultilineText)]
        public string SolutionDescription { get; set; }

        [Display(Name = "Nuværende systemer der understøtter processen")]
        public string CurrentSystems { get; set; }

        [Display(Name = "Antal gange processen foretages årligt")]
        public string AmountOfProcessExecutionsYearly { get; set; }

        [Display(Name = "Tidsforbrug pr. proces i minutter")]
        public string ProcessExecutionTime { get; set; }

        [Display(Name = "Antal medarbejdere der foretager processen")]
        public string AmountOfEmployees { get; set; }

        [Display(Name = "Total timeforbrug pr. år")]
        public string ProcessExecutionTimeHoursYearly { get; set; }

        [Display(Name = "Hvornår på året foretages processen")]
        public string TimeOfExecution { get; set; }

        [Display(Name = "Antal borgere/virksomheder der er berørte af processen")]
        public string AmountOfCitizensOrCompaniesAffected { get; set; }

        [Display(Name = "Er data og informationer, der skal bruges i processen tilgængelige digitalt i IT-systemer")]
        public string IsDataAvailableInSystems { get; set; }

        [Display(Name = "Kan automatisering bidrage til højere kvalitet eller højere service?")]
        public string AutomationWouldOptimizeTheProcess { get; set; }

        [Display(Name = "Andet", Description = "Noter gerne yderligere information omkring processen")]
        [DataType(DataType.MultilineText)]
        public string AdditionalInformation { get; set; }

        [Display(Name = "Bilag", Description = "F.eks. procestegninger, skærmbilleder eller anden dokumentation af processen. Hold CTRL nede for at markere flere filer.")]
        public IEnumerable<HttpPostedFileBase> UploadedFile { get; set; }

        [Display(Name = "Vurderet potentiale")]
        public string EstimatedPotential { get; set; }

        [Display(Name = "Erfaringer (evt. kontaktperson)")]
        [DataType(DataType.MultilineText)]
        public string Experience { get; set; }

        [Display(Name = "Forventet start")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EstimatedStartDate { get; set; }

        [Display(Name = "Forventet slut")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EstimatedEndDate { get; set; }

        [Display(Name = "Type af procesløsning")]
        [DataType(DataType.MultilineText)]
        public string TypeOfSolution { get; set; }

        [Display(Name = "Forslag til teknologi og løsning")]
        [DataType(DataType.MultilineText)]
        public string SolutionTechnologies { get; set; }

        [Display(Name = "Risiko og opmærksomhedspunkter")]
        [DataType(DataType.MultilineText)]
        public string Risk { get; set; }

        [Display(Name = "Yderligere oplysninger")]
        [DataType(DataType.MultilineText)]
        public string AdditionalInformationProjectGroup { get; set; }

        [Display(Name = "Bilag")]
        public virtual ICollection<AttachmentViewModel> Attachment { get; set; }
    }
}