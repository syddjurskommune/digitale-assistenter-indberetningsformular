﻿var lastHeaderIndex = $(".jquery-datatable").find("th").length - 1;

$(".jquery-datatable").dataTable({
    "language": {
        "url": "/Content/Scripts/Libraries/datatables.danish.json"
    },
    "order": [[1, "asc"]],
    "columnDefs": [{ "targets": 0, "sortable": false }, { "targets": lastHeaderIndex, "sortable": false, "width": "11%" }]
});