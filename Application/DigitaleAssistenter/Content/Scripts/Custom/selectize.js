﻿$.validator.setDefaults({
    ignore: ":hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input"
});

$("select[class*='selectize-autocomplete']").each(function () {
    var selectize = $(this).selectize({
        theme: "bootstrap",
        onChange: function (value) {
            $(selectize).valid();
        }
    });
});

$("#OrganizationsSelectize").selectize({
    maxItems: null,
    delimiter: ",",
    closeAfterSelect: true,
    onInitialize: function () {
        var initialValues = $("#OrganizationsSelectize").data("initial-value");
        var initialValuesSplitted = initialValues.toString().split(",");

        if (initialValuesSplitted.length > 0) {
            for (var i = 0; i < initialValuesSplitted.length; i++) {
                this.addItem(initialValuesSplitted[i], true);
            }
        } else {
            this.addItem(initialValues, true);
        }
    },
    onChange: function (value) {
        $("#OrganizationsSelectize").valid();
        $("#Organizations").val(value);
    }
});