﻿$.validator.methods.date = function (value, element) {
    return this.optional(element) || Date.parse(value) !== NaN;
};