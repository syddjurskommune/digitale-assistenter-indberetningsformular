﻿$(".delete-attachment").click(function (e) {
    e.preventDefault();

    var attachmentId = $(this).data("attachment-id");

    $.ajax({
        url: "/Attachment/DeleteAttachment",
        type: "POST",
        dataType: "html",
        data: { attachmentId: attachmentId },
        success: function (data) {
            $(".attachments").html(data);
        }
    });
});