﻿using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using DigitaleAssistenter.Models;

namespace DigitaleAssistenter.Classes
{
    public static class UserInfoHelpers
    {
        public static string GetDisplayName()
        {
            var domain = new PrincipalContext(ContextType.Domain);
            var currentUser = UserPrincipal.FindByIdentity(domain, HttpContext.Current.User.Identity.Name);

            return currentUser == null ? string.Empty : currentUser.DisplayName;
        }

        public static int GetUserId()
        {
            var domain = new PrincipalContext(ContextType.Domain);
            var currentUser = UserPrincipal.FindByIdentity(domain, HttpContext.Current.User.Identity.Name);

            using (var db = new DigitaleAssistenterEntities())
            {
                var user = db.EmployeeView.SingleOrDefault(m => m.Initials == currentUser.SamAccountName);

                if (user == null)
                {
                    return 0;
                }

                return user.EmployeeId;
            }
        }

    }
}