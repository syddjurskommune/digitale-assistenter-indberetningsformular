﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DigitaleAssistenter.Classes;
using DigitaleAssistenter.Models;
using DigitaleAssistenter.ViewModels;

namespace DigitaleAssistenter.Controllers
{
    public class AttachmentController : Controller
    {
        private readonly DigitaleAssistenterEntities _db = new DigitaleAssistenterEntities();
        private readonly int _userId = UserInfoHelpers.GetUserId();
        private readonly bool _isAdmin = System.Web.HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["AdminGroup"]);

        public ActionResult DownloadAttachment(int attachmentId)
        {
            var attachment = _db.Attachment.SingleOrDefault(a => a.AttachmentId == attachmentId);

            if (attachment == null) return HttpNotFound();
            if (!_isAdmin && attachment.Process.ConfidentialProcess == true && attachment.Process.CreatedByEmployeeId != _userId && attachment.Process.ReporterEmployeeId != _userId) return new HttpUnauthorizedResult();

            byte[] fileBytes = attachment.File;
            string fileName = attachment.FileName;

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult DeleteAttachment(int attachmentId)
        {
            var attachment = _db.Attachment.SingleOrDefault(a => a.AttachmentId == attachmentId);

            if (attachment == null) return HttpNotFound();
            if (!_isAdmin && attachment.Process.CreatedByEmployeeId != _userId && attachment.Process.ReporterEmployeeId != _userId) return new HttpUnauthorizedResult();

            _db.Attachment.Remove(attachment);
            _db.SaveChanges();

            var attachments = Mapper.Map<List<AttachmentViewModel>>(_db.Attachment.Where(a => a.ProcessId == attachment.ProcessId).ToList());

            return PartialView("PartialViews/_Attachments", attachments);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}