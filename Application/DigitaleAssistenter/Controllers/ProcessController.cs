﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DigitaleAssistenter.Models;
using DigitaleAssistenter.ViewModels;
using System.IO;
using DigitaleAssistenter.Classes;

namespace DigitaleAssistenter.Controllers
{
    public class ProcessController : Controller
    {
        private readonly DigitaleAssistenterEntities _db = new DigitaleAssistenterEntities();
        private readonly int _userId = UserInfoHelpers.GetUserId();
        private readonly bool _isAdmin = System.Web.HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["AdminGroup"]);

        public ActionResult Index()
        {
            var processes = Mapper.Map<List<ProcessDisplayViewModel>>(_db.ProcessView.ToList());

            return View(processes);
        }
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ProcessView process = _db.ProcessView.SingleOrDefault(p => p.ProcessId == id);

            if (process == null) return HttpNotFound();
            if (!_isAdmin && process.ConfidentialProcess == true && process.CreatedByEmployeeId != _userId && process.ReporterEmployeeId != _userId) return new HttpUnauthorizedResult();

            var processViewModel = Mapper.Map<ProcessDisplayViewModel>(process);

            processViewModel.Attachment = Mapper.Map<List<AttachmentViewModel>>(_db.Attachment.Where(a => a.ProcessId == process.ProcessId).ToList());

            return View(processViewModel);
        }
        public ActionResult Create()
        {
            var processViewModel = new ProcessViewModel
            {
                ReporterEmployeeId = _userId
            };

            PopulateViewBag();

            return View("Form", processViewModel);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Process process = _db.Process.Find(id);

            if (process == null) return HttpNotFound();
            if (!_isAdmin && process.CreatedByEmployeeId != _userId && process.ReporterEmployeeId != _userId) return new HttpUnauthorizedResult();

            var processViewModel = Mapper.Map<ProcessViewModel>(process);

            PopulateViewBag(id);

            return View("Form", processViewModel);
        }

        [AuthorizeByConfig(RolesAppSettingKey = "AdminGroup")]
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Process process = _db.Process.Find(id);

            if (process == null) return HttpNotFound();

            var processViewModel = Mapper.Map<ProcessViewModel>(process);

            return View(processViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProcessId,KLEId,ContactEmployeeId,ReporterEmployeeId,CreatedByEmployeeId,ProfessionalAssessmentRatingId,FrequentChangesRatingId,StructuredDataRatingId,VariationRatingId,StatusId,Name,Organizations,Resume,Paragraph,ConfidentialProcess,Description,SolutionDescription,AmountOfProcessExecutionsYearly,TimeOfExecution,ProcessExecutionTime,ProcessExecutionTimeHoursYearly,AmountOfEmployees,AmountOfCitizensOrCompaniesAffected,IsDataAvailableInSystems,CurrentSystems,AutomationWouldOptimizeTheProcess,AdditionalInformation,UploadedFile,EstimatedPotential,Experience,EstimatedStartDate,EstimatedEndDate,TypeOfSolution,SolutionTechnologies,Risk,AdditionalInformationProjectGroup")] ProcessViewModel processViewModel)
        {
            if (ModelState.IsValid)
            {
                var process = Mapper.Map<Process>(processViewModel);

                process.CreatedByEmployeeId = _userId;

                _db.Process.Add(process);
                _db.SaveChanges();

                SaveOrganizations(processViewModel.Organizations, process.ProcessId);
                SaveAttachments(processViewModel.UploadedFile, process.ProcessId);

                return RedirectToAction("Index");
            }

            PopulateViewBag();

            return View("Form", processViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProcessId,KLEId,ContactEmployeeId,ReporterEmployeeId,CreatedByEmployeeId,ProfessionalAssessmentRatingId,FrequentChangesRatingId,StructuredDataRatingId,VariationRatingId,StatusId,Name,Organizations,Resume,Paragraph,ConfidentialProcess,Description,SolutionDescription,AmountOfProcessExecutionsYearly,TimeOfExecution,ProcessExecutionTime,ProcessExecutionTimeHoursYearly,AmountOfEmployees,AmountOfCitizensOrCompaniesAffected,IsDataAvailableInSystems,CurrentSystems,AutomationWouldOptimizeTheProcess,AdditionalInformation,UploadedFile,EstimatedPotential,Experience,EstimatedStartDate,EstimatedEndDate,TypeOfSolution,SolutionTechnologies,Risk,AdditionalInformationProjectGroup")] ProcessViewModel processViewModel)
        {
            if (ModelState.IsValid)
            {
                var process = Mapper.Map<Process>(processViewModel);

                _db.Entry(process).State = EntityState.Modified;
                _db.ProcessOrganization.RemoveRange(_db.ProcessOrganization.Where(po => po.ProcessId == process.ProcessId));
                _db.SaveChanges();

                SaveOrganizations(processViewModel.Organizations, process.ProcessId);
                SaveAttachments(processViewModel.UploadedFile, process.ProcessId);

                return RedirectToAction("Index");
            }

            PopulateViewBag(processViewModel.ProcessId);

            return View("Form", processViewModel);
        }

        [AuthorizeByConfig(RolesAppSettingKey = "AdminGroup")]
        [ActionName("Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Process process = _db.Process.SingleOrDefault(p => p.ProcessId == id);

            if (process == null) return HttpNotFound();

            _db.ProcessOrganization.RemoveRange(process.ProcessOrganization);
            _db.Attachment.RemoveRange(process.Attachment);
            _db.Process.Remove(process);
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected void PopulateViewBag(int? processId = null)
        {
            ViewBag.Employees = new SelectList(_db.EmployeeView.OrderBy(e => e.Name), "EmployeeId", "InitialsAndName");
            ViewBag.KLE = new SelectList(_db.KLEView.OrderBy(k => k.KLENumberText), "KLEId", "KLENumberText");
            ViewBag.Organizations = new SelectList(_db.OrganizationView.OrderBy(o => o.Name), "OrganizationId", "Name");
            ViewBag.Ratings = _db.Rating.OrderByDescending(r => r.Score).ToList();
            ViewBag.Status = _db.Status.ToList();

            if (processId.HasValue)
            {
                ViewBag.Attachments = Mapper.Map<List<AttachmentViewModel>>(_db.Attachment.Where(a => a.ProcessId == processId).ToList());
            }
        }
        protected void SaveOrganizations(string organizations, int processId)
        {
            var organizationsSplitted = organizations.Split(',');

            foreach (var organization in organizationsSplitted)
            {
                _db.ProcessOrganization.Add(new ProcessOrganization { OrganizationId = int.Parse(organization), ProcessId = processId });
            }

            _db.SaveChanges();
        }
        protected void SaveAttachments(IEnumerable<HttpPostedFileBase> uploadedFile, int processId)
        {
            if (uploadedFile == null || !uploadedFile.Any(a => a != null && a.ContentLength > 0)) return;

            foreach (var file in uploadedFile)
            {
                MemoryStream memoryStream = new MemoryStream();

                file.InputStream.CopyTo(memoryStream);

                _db.Attachment.Add(new Attachment { FileName = file.FileName, ContentType = file.ContentType, File = memoryStream.ToArray(), ProcessId = processId });
            }

            _db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
