﻿using System.Linq;
using AutoMapper;
using DigitaleAssistenter.ViewModels;

namespace DigitaleAssistenter
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Models.Process, ProcessViewModel>().ForMember(x => x.Organizations, opt => opt.MapFrom(src => string.Join(",", src.ProcessOrganization.Select(po => po.OrganizationId))));
                cfg.CreateMap<ProcessViewModel, Models.Process>();

                cfg.CreateMap<Models.ProcessView, ProcessDisplayViewModel>();

                cfg.CreateMap<Models.Attachment, AttachmentViewModel>();
                cfg.CreateMap<AttachmentViewModel, Models.Attachment>();

                cfg.CreateMap<Models.ProcessOrganization, ProcessOrganizationViewModel>();
                cfg.CreateMap<ProcessOrganizationViewModel, Models.ProcessOrganization>();
            });
        }
    }
}
