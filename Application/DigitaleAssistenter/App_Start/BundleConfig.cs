﻿using System.Web;
using System.Web.Optimization;

namespace DigitaleAssistenter
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/Scripts/Libraries/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/Scripts/Libraries/jquery.validate*",
                        "~/Content/Scripts/Custom/jquery.validate.override.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/Scripts/Libraries/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/Scripts/Libraries/bootstrap.js",
                      "~/Content/Scripts/Libraries/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datepicker").Include(
                "~/Content/Scripts/Libraries/bootstrap-datepicker.min.js",
                "~/Content/Scripts/Libraries/bootstrap-datepicker.da.min.js",
                "~/Content/Scripts/Custom/bootstrap-datepicker.js"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/Styles/Libraries/bootstrap-flatly-theme.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/Styles/Libraries/bootstrap-datepicker3.standalone.min.css",
                         "~/Content/Styles/Custom/site.css"));
        }
    }
}
